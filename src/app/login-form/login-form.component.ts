import { Component, inject } from '@angular/core';
import { User } from '../utils/user';
import { Router } from '@angular/router';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  constructor(private router:Router) { }

  model = new User(-1, '', '');

  submitted = false;
  wrongCreds = false;

  onSubmit() { 
    this.submitted = true;
    this.connect();
  }

  closeError() {
    this.wrongCreds = false;
  }

  async connect() {
    const lockedUser = this.model; 
    
    const response = await fetch('http://localhost:3000/login', {
      method: 'POST',
      headers: {"Content-Type": "application/json"},
      body: `${JSON.stringify({"username" : lockedUser.username, "password" : lockedUser.password})}`
    });

    switch(await response.status) {
      case 200 : 
        const data = await response.json(); 
        const accountId = await data[0].id; 
        sessionStorage.setItem('accountId', accountId);
        console.log(sessionStorage.getItem('accountId'));
        this.wrongCreds = false; 
        this.router.navigate(['/waiting']);
        break;
      case 401 : 
        this.wrongCreds = true; 
        break;
    }
    
    this.submitted = false;
  }
}